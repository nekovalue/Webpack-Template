import './main'
import '../../index.html'
import '../sass/main.scss'

import '../../../node_modules/axios/dist/axios'

window.Vue = require('vue/dist/vue.js')
import store from '../vue_elements/store'

const App = new Vue({
    data: {},
    store,
    el: '#app'
})